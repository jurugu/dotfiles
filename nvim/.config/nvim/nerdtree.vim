#!/bin/bash

" NERDTree
nmap <C-n> :NERDTreeToggle<CR>
nmap <C-l> :tabn<CR>
nmap <C-h> :tabp<CR>

let NERDTreeShowHidden=1
