call plug#begin('~/.vim/plugged')

" Navigation tree
Plug 'preservim/nerdtree'

" Autocomplete
"Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
Plug 'neoclide/coc.nvim', {'branch': 'release'}

" Python Docstrings
Plug 'kkoomen/vim-doge', { 'do': { -> doge#install() } }

" Vim airline
Plug 'tpope/vim-fugitive'

" Fuzzy finder
Plug 'ctrlpvim/ctrlp.vim'

" Prettyfying stuff
Plug 'vim-airline/vim-airline'
Plug 'ryanoasis/vim-devicons'

" Git related
Plug 'Xuyuanp/nerdtree-git-plugin'

" Latex
Plug 'lervag/vimtex'

" Color preview
Plug 'ap/vim-css-color'

" Fuzzy finder
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf.vim'

" Vimwiki
Plug 'vimwiki/vimwiki'

" Firenvim
Plug 'glacambre/firenvim', { 'do': { _ -> firenvim#install(0) } }



call plug#end()


" Vim specific
:set splitright
:set splitbelow

" reference: https://gist.github.com/synasius/5cdc75c1c8171732c817
set tabstop=4
set softtabstop=4
set shiftwidth=4
set expandtab

" Autocomplete
"let g:deoplete#enable_at_startup = 1

" Jefi conf
"let g:deoplete#sources#jedi#show_docstring = 1

" Docstrings conf
" default <Leader>d to generate
let g:doge_doc_standard_python = 'numpy'

" Python config for venvs
let g:python_host_prog = '/home/jurugu/miniconda3/envs/neovim2/bin/python'
let g:python3_host_prog = '/home/jurugu/miniconda3/envs/neovim3/bin/python'

"Vim configs
set nu

" vimtex
let g:vimtex_compiler_progname = 'nvr'
let g:vimtex_view_method = 'zathura'

" vimwiki
set nocompatible
filetype plugin on
syntax on

" Source external
source $HOME/.config/nvim/coc.vim
source $HOME/.config/nvim/devicons.vim
source $HOME/.config/nvim/latex-preview.vim
source $HOME/.config/nvim/nerdtree.vim
source $HOME/.config/nvim/ctrlp.vim
