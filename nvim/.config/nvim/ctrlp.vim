#!/bin/bash

let g:ctrlp_show_hidden = 1
let g:ctrlp_custom_ignore = 'node_modules\|git'
